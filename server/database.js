//Loads Sequelize
var Sequelize = require("sequelize");

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'wAve1';

// Creates a MySQL connection
var sequelize = new Sequelize(
    "ellery_db",
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         // default port    : 3306
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

//Load models for ellery_db
var Product = require("./models/product")(sequelize, Sequelize);
//var Promo = require("./models/promo")(sequelize, Sequelize);
//var Cart = require("./models/cart")(sequelize, Sequelize);
//var Cart_Product

database.sync()
    .then(function () {
        console.log("DB in sync")
    });

module.exports = {
    ellery: elleryModel
};

