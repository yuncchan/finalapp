// Always use an IIFE, i.e., (function() {})();
    // Creates a new module
    // When setting (creating) an angular module, you need to specify the second argument ([ ... ])
    // Without this argument, we are telling Angular that what we want to do is to get an already existing module
  
(function () {
    angular.module("ellery", ['ui.router', 'ui.select', "ngMessages"]);
})();